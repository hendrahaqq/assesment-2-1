package main

import "fmt"

func countAll(slice []float32)(float32, float32, float32, float32){
	min := slice[0]
	max := slice[0]
	var total, average float32

	for i, v := range slice {
	   fmt.Println("index ", i, " value ", v)
	   total += v
	   if v < min {
		   min = v
	   }
	   if v > max {
		   max = v
	   }
   }
   average = total/float32(len(slice))
   return min, max, total, average
 }


func main(){
 	var nilai = [...]float32{
		23, 45, 67, 54, 66, 19, 56, 78, 89, 44, 11, 22, 33, 44, 55, 66, 77, 88, 99, 23, 34, 32, 23, 12,
	 }

	 fmt.Println(nilai)


	 //kumpulan ke 1
	 fmt.Println("Ini adalah kumpulan 1:")
	 var slice1 = nilai[0:8]
	 minimal, maksimal, total1, average := countAll(slice1) 
	 fmt.Println("nilai minimal kumpulan 1 :", minimal)
	 fmt.Println("nilai maksimal kumpulan 1 :",maksimal)
	 fmt.Println("nilai total kumpulan 1 :",total1)
	 fmt.Printf("nilai rata-rata kumpulan 1 : %.2f\n", average)

	//  kumpulan ke 2
	fmt.Println("Ini adalah kumpulan 2:")
	 var slice2 = nilai[8:16]
	 minimal2, maksimal2, total2, average2 := countAll(slice2) 
	 fmt.Println("nilai minimal kumpulan 2 :", minimal2)
	 fmt.Println("nilai maksimal kumpulan 2 :", maksimal2)
	 fmt.Println("nilai total kumpulan 2 :", total2)
	 fmt.Printf("nilai rata-rata kumpulan 2 : %.2f\n", average2)

	 //  kumpulan ke 2
	fmt.Println("Ini adalah kumpulan 3:")
	var slice3 = nilai[16:24]
	minimal3, maksimal3, total3, average3 := countAll(slice3) 
	fmt.Println("nilai minimal kumpulan 3 :", minimal3)
	fmt.Println("nilai maksimal kumpulan 3 :", maksimal3)
	fmt.Println("nilai total kumpulan 3 :", total3)
	fmt.Printf("nilai rata-rata kumpulan 3 : %.2f\n", average3)

	// var totalArray = [3]float32{
	// 	total1,
	// 	total2,
	// 	total3,
	// }

	// fmt.Println(totalArray)

	// nilai tertinggi
	if total3 < total1 && total1 > total2 {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 1", slice1, "dengan total", total1)
	} else if total3 < total2 && total2 > total3 {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("\nkumpulan dengan total tertinggi adalah slice 3", slice3, "dengan total", total3)
	}

	//nilai terendah
	if total3 > total1 && total1 < total2 {
		fmt.Println("kumpulan dengan total terendah adalah slice 1", slice1, "dengan total", total1)
	} else if total3 > total2 && total2 < total3 {
		fmt.Println("kumpulan dengan total terendah adalah slice 2", slice2, "dengan total", total2)
	} else {
		fmt.Println("kumpulan dengan total terendah adalah slice 3", slice3, "dengan total", total3)
	}
}